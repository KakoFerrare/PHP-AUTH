<?php
namespace ETI\Auth;
/**
 * Faz a Autenticação dos Usuários
 */

Class AuthUser{
    
    /**
     *
     * @var \PDO Objeto de conexão
     */
    private $conn;
    
    /**
     * DI - Precisa se conectar ao banco de dados
     * @param \PDO $pdo Objeto de conexão
     */
    public function __construct(\PDO $pdo) {
        $this->conn = $pdo;
    }
    
    /**
     * Faz a autenticação do usuário no sistema
     * 
     * @param string $email E-mail do Usuário
     * @param string $password Senha do Usuário
     * 
     * @return boolean True Success or False Fail
     */
    public function authUser($email, $password){
        return true;
    }
    
    public function checkUserLogin(){
        return true;
    }
}