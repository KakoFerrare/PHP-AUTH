<?php

class PDOConnect{
    //MODELO SINGLETON, VERIFICA SE A CONEXÃO JA FOI ESTABELECIDA, SE JA FOI CRIADO UM OBJETO DE 
    //CONEXÃO ELE NÃO VAI SER ESTANCIADO NOVAMENTE
    
    //VERSÃO 2 - DEIXEI O MÉTODO E VARIAVEL ESTATICOS, PARA ACHAR O OBJETO A PARTIR DA CLASSE
    //SEM PRECISAR ESTANCIAR
    private static $conn = null;

    public static function connect(){
        //new PDO($dsn, $username, $password, $options)
        // sdn = o banco que vou estar utilizando
        try{
            //PARA ACESSAR ATRIBUTI ESTATICO self::<atributo>
            if(self::$conn === null){
            //MODO MANUAL DE DEFINIR MEUS PARAMETROS DE
            //$this->conn = new PDO("mysql:host=localhost",'root' ,'fcm020964');
                self::$conn = new PDO(DSN.':host='.HOST.';dbname='.DB, USER, PASSWORD);
            }
            return self::$conn;
        }
        catch (PDOException $e){
            echo "Falha ao conectar com o banco de dados. {$e->getMessage()}";
        }
    }
    
}